//
//  Pokemon.swift
//  pokemonGame
//
//  Created by Formando ATEC on 07/10/2021.
//

import Foundation

class Ability {
    private var id:Int
    private var name:String
    private var base_value:Int
    private var type:String
    private var min_level:Int 
    
    
    
    init(id:Int, name:String, base_value:Int, type:String, min_level:Int) {
        self.id = id
        self.name = name
        self.base_value = base_value
        self.type = type
        self.min_level = min_level
    }
    
    //Getters
    func getId() -> Int{
        return self.id
    }

    func getName() -> String{
        return self.name
    }

    func getBase_value() -> Int{
        return self.base_value
    }

    func getType() -> String{
        return self.type
    }

    func getMin_level() -> Int{
        return self.min_level
    } 

    //Setters
    func setId(id:Int){
        self.id = id
    }

    func setName(name:String){
        self.name = name
    }

    func setBase_value(base_value:Int){
        self.base_value = base_value
    }
    
    func setType(type:String){
        self.type = type
    }

    func setMin_level(min_level:Int){
        self.min_level = min_level
    }

}
