//
//  Pokemon.swift
//  pokemonGame
//
//  Created by Formando ATEC on 07/10/2021.
//

import Foundation

class Pokemon {
    private var id:Int
    private var name:String
    private var level:Int
    private var xp:Int
    private var type:String
    private var hp:Int
    private var attack:Int
    private var defense:Int
    private var special_attack:Int
    private var special_defense:Int
    private var speed:Int
    private var abilities:[Int]
        
    init(id:Int, name:String, level:Int, xp:Int, type:String, hp:Int, attack:Int, defense:Int, special_attack:Int, special_defense:Int, speed:Int, abilities:[Int]) {
        self.id = id
        self.name = name
        self.level = level
        self.xp = xp
        self.type = type
        self.hp = hp
        self.attack = attack
        self.defense = defense
        self.special_attack = special_attack
        self.special_defense = special_defense
        self.speed = speed
        self.abilities = abilities
    }
    
    func getId() -> Int{
        return self.id
    }
    func getName() -> String{
        return self.name
    }
    
    func getLevel() -> Int{
        return self.level	
    }
    
    func getXp() -> Int{
        return self.xp
    }
    
    func getType() -> String{
        return self.type
    }
    
    func getHp() -> Int{
        return self.hp
    }
    
    func getAttack() -> Int{
        return self.attack
    }
    
    func getDefense() -> Int{
        return self.defense
    }
    
    func getSpecial_Attack() -> Int{
        return self.special_attack
    }
    
    func getSpecial_Defense() -> Int{
        return self.special_defense
    }

    func getSpeed() -> Int{
        return self.speed
    }

    func getAbilities() -> [Int]{
        return self.abilities
    }

    func setId(id:Int){
        self.id = id
    }
    
    func setName(name:String){
        self.name = name
    }

    func setLevel(level:Int){
        self.level = level
    }

    func setXp(xp:Int){
        self.xp = xp
    }

    func setType(type:String){
        self.type = type
    }

    func setHp(hp:Int){
        self.hp = hp
    }

    func setAttack(attack:Int){
        self.attack = attack
    }

    func setDefense(defense:Int){
        self.defense = defense
    }

    func setSpecial_Attack(special_attack:Int){
        self.special_attack = special_attack
    }

    func setSpecial_Defense(special_defense:Int){
        self.special_defense = special_defense
    }

    func setSpeed(speed:Int){
        self.speed = speed
    }

    func setAbilities(abilities:[Int]){
        self.abilities = abilities
    }
}
