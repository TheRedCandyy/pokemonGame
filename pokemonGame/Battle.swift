//
//  Battle.swift
//  pokemonGame
//
//  Created by Formando ATEC on 27/10/2021.
//

import Foundation

class Battle{
    private var id:Int
    private var winner:Pokemon
    private var loser:Pokemon
    private var xpWon:Int
    private var rounds:Int
    
    init(id:Int, winner:Pokemon, loser:Pokemon, xpWon:Int, rounds:Int){
        self.id = id
        self.winner = winner
        self.loser = loser
        self.xpWon = xpWon
        self.rounds = rounds
    }
    
    func getId() -> Int{
        return self.id
    }
    
    func getWinner() -> Pokemon {
        return self.winner
    }
    
    func getLoser() -> Pokemon {
        return self.loser
    }
    
    func getXpWon() -> Int {
        return self.xpWon
    }
    
    func getRounds() -> Int {
        return self.rounds
    }
    
    func setId(id:Int) {
        self.id = id
    }
    
    func setWinner(winner:Pokemon) {
        self.winner = winner
    }
    
    func setLoser(loser:Pokemon) {
        self.loser = loser
    }
    
    func setXpWon(xpWon:Int) {
        self.xpWon = xpWon
    }
    
    func setRounds(rounds:Int) {
        self.rounds = rounds
    }
}
