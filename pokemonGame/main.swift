//
//  main.swift
//  pokemonGame
//
//  Created by Formando ATEC on 07/10/2021.
//

import Foundation

var escolha:Int
var close:Bool

//Constante que contem o menu principal do jogo
let Menu:[String] = [
    "+----------------------------------+",
    "|               MENU               |",
    "+----------------------------------+",
    "|                                  |",
    "|                                  |",
    "|   1 -  Create Pokemon            |",
    "|   2 -  Edit Pokemon              |",
    "|   3 -  Delete Pokemon            |",
    "|   4 -  Start Battle              |",
    "|   5 -  Battle History            |",
    "|                                  |",
    "|                                  |",
    "|----------------------------------|",
    "|  0 - Leave App                   |",
    "+----------------------------------+",
    "        Escolha : ",
]

let pokemonTypes:[String] = [
    "╔══════════════════════════════════╗",
    "║       ✪✧✪   TYPES   ✪✧✪          ║",
    "╠══════════════════════════════════╣",
    "║                                  ║",
    "║        ESCOLHA UM TIPO           ║",
    "║                                  ║",
    "║   1 -  Water                     ║",
    "║   2 -  Rock                      ║",
    "║   3 -  Fire                      ║",
    "║   4 -  Electric                  ║", //Sub menu de Iniciar Batalha
    "║   5 -  Steel                     ║",
    "║   6 -  Grass                     ║",
    "║   7 -  Poison                    ║",
    "║   8 -  Ghost                     ║",
    "║   9 -  Dragon                    ║",
    "║  10 -  Fairy                     ║",
    "╠══════════════════════════════════╣",
    "║                                  ║",
    "║   0 - Exit                       ║",
    "║                                  ║",
    "╚══════════════════════════════════╝",
]

let campoAEditar:[String] = [
    "╔══════════════════════════════════╗",
    "║        Pokemon Atributes         ║",
    "╠══════════════════════════════════╣",
    "║                                  ║",
    "║     Choose The Fiel to Edit      ║",
    "║                                  ║",
    "║   1 -  Name                      ║",
    "║   2 -  Type                      ║",
    "║   3 -  Attack                    ║",
    "║   4 -  Defense                   ║",
    "║   5 -  Special Attack            ║",
    "║   6 -  Special Defense           ║",
    "║                                  ║",
    "╠══════════════════════════════════╣",
    "║                                  ║",
    "║   0 - Exit                       ║",
    "║                                  ║",
    "╚══════════════════════════════════╝",
]

let choseGenericAbilitie:[String] = [
    "╔══════════════════════════════════════════════════════════════════════╗",
    "║                          Gerenric Abilities                          ║",
    "╠══════════════════════════════════════════════════════════════════════╣",
    "║                                                                      ║",
    "║                         Choose ONE Abilitie                          ║",
    "║                                                                      ║",
    "║                                                                      ║",
    "║         NAME                                BASE DAMAGE              ║",
    "║                                                                      ║",
    "║                                                                      ║",
    "║   1 -  Tackle  ________________________________  10                  ║",
    "║   2 -  Kick  __________________________________  12                  ║",
    "║   3 -  Head Bump  _____________________________  10                  ║",
    "║   4 -  Push  __________________________________  14                  ║",
    "║   5 -  Slap  __________________________________  09                  ║",
    "║   6 -  Knee Kick  _____________________________  11                  ║",
    "║                                                                      ║",
    "║                                                                      ║",
    "║                                                                      ║",
    "╠══════════════════════════════════════════════════════════════════════╣",
    "║                                                                      ║",
    "║   0 - Exit                                                           ║",
    "║                                                                      ║",
    "╚══════════════════════════════════════════════════════════════════════╝",
]

//Habilidades Genericas
let tackle:Ability = Ability(id:1, name:"Tackle", base_value:10, type:"Normal", min_level:0)
let kick:Ability = Ability(id:2, name:"Kick", base_value:10, type:"Normal", min_level:0)
let headBump:Ability = Ability(id:3, name:"Head Bump", base_value:10, type:"Normal", min_level:0)
let push:Ability     = Ability(id:4, name:"Push",      base_value:14, type:"Normal", min_level:0)
let slap:Ability     = Ability(id:5, name:"Slap",      base_value:9,  type:"Normal", min_level:0)
let kneeKick:Ability = Ability(id:6, name:"Knee Kick", base_value:11, type:"Normal", min_level:0)
//_____________________________________________________________________________________________________________________
//Habilidades de Agua
let waterJet:Ability        = Ability(id:7,  name:"Water Jet",         base_value:15, type:"Water", min_level:3 ) 
let bubleBeam:Ability       = Ability(id:8,  name:"Bubble Beam",       base_value:13, type:"Water", min_level:3 ) 
let surf:Ability            = Ability(id:9,  name:"Surf",              base_value:15, type:"Water", min_level:3 )
let originPulse:Ability     = Ability(id:10,  name:"Origin Pulse",     base_value:15, type:"Water", min_level:3 )
let hydroPump:Ability       = Ability(id:11,  name:"Hydro Pump",       base_value:35, type:"Water", min_level:9 ) 
let waterfall:Ability       = Ability(id:12, name:"Waterfall",         base_value:40, type:"Water", min_level:9 ) 
let sparklingAria:Ability   = Ability(id:13, name:"Sparkling Aria",    base_value:37, type:"Water", min_level:9 ) 
let crabhammer:Ability      = Ability(id:14, name:"Crabhammer",        base_value:51, type:"Water", min_level:9 ) 
let aquaTail:Ability        = Ability(id:15, name:"Aqua Tail",         base_value:80, type:"Water", min_level:16 ) 
let maxGeyser:Ability       = Ability(id:16, name:"Max Geyser",        base_value:85, type:"Water", min_level:16 ) 
let steamEruption:Ability   = Ability(id:17, name:"Steam Eruption",    base_value:83, type:"Water", min_level:16 ) 
let hydroCannon:Ability     = Ability(id:18, name:"Hydro Cannon",      base_value:91, type:"Water", min_level:16 ) 

//_____________________________________________________________________________________________________________________
//Habilidades de Rock
let rockHead:Ability        = Ability(id:19, name:"Rock Head",         base_value:15, type:"Rock", min_level:3 ) 
let magnetPull:Ability      = Ability(id:20, name:"Magnet Pull",       base_value:17, type:"Rock", min_level:3 ) 
let rockThrow:Ability       = Ability(id:21, name:"Rock Throw",        base_value:21, type:"Rock", min_level:3 )
let rockBlast:Ability       = Ability(id:22, name:"Rock Blast",        base_value:25, type:"Rock", min_level:3 )
let levitate:Ability        = Ability(id:23, name:"Levitate",          base_value:35, type:"Rock", min_level:9 ) 
let rockStorm:Ability       = Ability(id:24, name:"Rock Storm",        base_value:40, type:"Rock", min_level:9 ) 
let smackDown:Ability       = Ability(id:25, name:"Smack Down",        base_value:51, type:"Rock", min_level:9 ) 
let rollout:Ability         = Ability(id:26, name:"Rollout",           base_value:48, type:"Rock", min_level:9 ) 
let rockShield:Ability      = Ability(id:27, name:"Rock Shield",       base_value:82, type:"Rock", min_level:16 ) 
let earthquake:Ability      = Ability(id:28, name:"Earthquake",        base_value:95, type:"Rock", min_level:16 ) 
let tarShot:Ability         = Ability(id:29, name:"Tar Shot",          base_value:91, type:"Rock", min_level:16 ) 
let stoneEdge:Ability       = Ability(id:30, name:"Stone Edge",        base_value:87, type:"Rock", min_level:16 ) 

//_____________________________________________________________________________________________________________________
//Habilidades de Fire
let blaze:Ability           = Ability(id:31, name:"Blaze",             base_value:15, type:"Fire", min_level:3 ) 
let steamEngine:Ability     = Ability(id:32, name:"Steam Engine",      base_value:20, type:"Fire", min_level:3 ) 
let flameWheel:Ability      = Ability(id:33, name:"Flame Wheel",       base_value:20, type:"Fire", min_level:3 ) 
let heatCrash:Ability       = Ability(id:34, name:"Heat Crash",        base_value:20, type:"Fire", min_level:3 ) 
let whiteSmoke:Ability      = Ability(id:35, name:"White Smoke",       base_value:40, type:"Fire", min_level:9 ) 
let turboBlaze:Ability      = Ability(id:36, name:"Turboblaze",        base_value:51, type:"Fire", min_level:9 ) 
let fusionFlare:Ability     = Ability(id:37, name:"Fusion Flare",      base_value:55, type:"Fire", min_level:9 ) 
let flamethrower:Ability    = Ability(id:38, name:"Flamethrower",      base_value:62, type:"Fire", min_level:9 ) 
let wallOfFire:Ability      = Ability(id:39, name:"Wall of Fire",      base_value:80, type:"Fire", min_level:16 ) 
let solarExplosion:Ability  = Ability(id:40, name:"Solar Explosion",   base_value:95, type:"Fire", min_level:16 ) 
let firePunch:Ability       = Ability(id:41, name:"Fire Punch",        base_value:95, type:"Fire", min_level:16 ) 
let fireSpin:Ability        = Ability(id:42, name:"Fire Spin",         base_value:95, type:"Fire", min_level:16 ) 

//_____________________________________________________________________________________________________________________
//Habilidades de Electric
let galvanize:Ability       = Ability(id:43, name:"Galvanize",         base_value:9, type:"Electric",  min_level:3 ) 
let thunderbolt:Ability     = Ability(id:44, name:"Thunderbolt",       base_value:13, type:"Electric", min_level:3 )  
let discharge:Ability       = Ability(id:45, name:"Discharge",         base_value:14, type:"Electric", min_level:3 )  
let electricTerrain:Ability = Ability(id:46, name:"Electric Terrain",  base_value:20, type:"Electric", min_level:3 ) 
let electricSurge:Ability   = Ability(id:47, name:"Electric Surge",    base_value:35, type:"Electric", min_level:9 ) 
let lightningRod:Ability    = Ability(id:48, name:"Lightning Rod",     base_value:47, type:"Electric", min_level:9 ) 
let gigavoltHavoc:Ability   = Ability(id:49, name:"Gigavolt Havoc",    base_value:43, type:"Electric", min_level:9 ) 
let spark:Ability           = Ability(id:50, name:"Spark",             base_value:39, type:"Electric", min_level:9 ) 
let voltAbsorb:Ability      = Ability(id:51, name:"Volt Absorb",       base_value:83, type:"Electric", min_level:16 ) 
let motorDrive:Ability      = Ability(id:52, name:"Motor Drive",       base_value:91, type:"Electric", min_level:16 )  
let thunderShock:Ability    = Ability(id:53, name:"Thunder Shock",     base_value:99, type:"Electric", min_level:16 )  
let voltTackle:Ability      = Ability(id:54, name:"Volt Tackle",       base_value:87, type:"Electric", min_level:16 ) 

//_____________________________________________________________________________________________________________________
//Habilidades de Steel
let metalBurst:Ability      = Ability(id:55, name:"Metal Burst",       base_value:5, type:"Steel",   min_level:3 ) 
let metalSound:Ability      = Ability(id:56, name:"Metal Sound",       base_value:21, type:"Steel",  min_level:3 ) 
let magnetBomb:Ability      = Ability(id:57, name:"Magnet Bomb",       base_value:14, type:"Steel",  min_level:3 ) 
let heavySlam:Ability       = Ability(id:58, name:"Heavy Slam",        base_value:17, type:"Steel",  min_level:3 ) 
let shiftGear:Ability       = Ability(id:59, name:"Shift Gear",        base_value:35, type:"Steel",  min_level:9 ) 
let steelRoller:Ability     = Ability(id:60, name:"Steel Roller",      base_value:57, type:"Steel",  min_level:9 )
let gearUp:Ability          = Ability(id:61, name:"Gear Up",           base_value:36, type:"Steel",  min_level:9 )
let doomDesire:Ability      = Ability(id:62, name:"Doom Desire",       base_value:38, type:"Steel",  min_level:9 ) 
let steelWing:Ability       = Ability(id:63, name:"Steel Wing",        base_value:84, type:"Steel",  min_level:16 ) 
let sunSteelStrike:Ability  = Ability(id:64, name:"Sun Steel Strike",  base_value:91, type:"Steel",  min_level:16 ) 
let bulletPunch:Ability     = Ability(id:65, name:"Bullet Punch",      base_value:97, type:"Steel",  min_level:16 ) 
let autotomize:Ability      = Ability(id:66, name:"Autotomize",        base_value:100, type:"Steel", min_level:16 ) 

//_____________________________________________________________________________________________________________________
//Habilidades de Grass
let cottonSpore:Ability     = Ability(id:67, name:"Cotton Spore",      base_value:7, type:"Grass", min_level:3 ) 
let cottonGuard:Ability     = Ability(id:68, name:"Cotton Guard",      base_value:11, type:"Grass", min_level:3 ) 
let leafStorm:Ability       = Ability(id:69, name:"Leaf Storm",        base_value:16, type:"Grass", min_level:3 ) 
let leafBlade:Ability       = Ability(id:70, name:"Leaf Blade",        base_value:18, type:"Grass", min_level:3 ) 
let bulletSeed:Ability      = Ability(id:71, name:"Bullet Seed",       base_value:22, type:"Grass", min_level:9 ) 
let overgrow:Ability        = Ability(id:72, name:"Overgrow",          base_value:24, type:"Grass", min_level:9 )
let ingrain:Ability         = Ability(id:73, name:"Ingrain",           base_value:27, type:"Grass", min_level:9 )
let needleArm:Ability       = Ability(id:74, name:"Needle Arm",        base_value:23, type:"Grass", min_level:9 )
let gigaDrain:Ability       = Ability(id:75, name:"Giga Drain",        base_value:60, type:"Grass", min_level:16 ) 
let frenzyPlant:Ability     = Ability(id:76, name:"Frenzy Plant",      base_value:63, type:"Grass", min_level:16 ) 
let magicalLeaf:Ability     = Ability(id:77, name:"Magical Leaf",      base_value:66, type:"Grass", min_level:16 ) 
let grassyTerrain:Ability   = Ability(id:78, name:"Grassy Terrain",    base_value:69, type:"Grass", min_level:16 ) 

//_____________________________________________________________________________________________________________________
//Habilidades de Poison
let poison:Ability          = Ability(id:79, name:"Poison",            base_value:15, type:"Poison", min_level:3 ) 
let poisonPoint:Ability     = Ability(id:80, name:"Poison Point",      base_value:22, type:"Poison", min_level:3 ) 
let poisonPowder:Ability    = Ability(id:81, name:"Poison Powder",     base_value:31, type:"Poison", min_level:3 ) 
let maxOoze:Ability         = Ability(id:82, name:"Max Ooze",          base_value:36, type:"Poison", min_level:3 ) 
let acid:Ability            = Ability(id:83, name:"Acid",              base_value:41, type:"Poison", min_level:9 ) 
let acidSpray:Ability       = Ability(id:84, name:"Acid Spray",        base_value:61, type:"Poison", min_level:9 ) 
let acidDownpour:Ability    = Ability(id:85, name:"Acid Downpour",     base_value:65, type:"Poison", min_level:9 ) 
let corrosiveGas:Ability    = Ability(id:86, name:"Corrosive Gas",     base_value:52, type:"Poison", min_level:9 ) 
let smog:Ability            = Ability(id:87, name:"Smog",              base_value:69, type:"Poison", min_level:16 ) 
let venoshock:Ability       = Ability(id:88, name:"Veno Shock",        base_value:80, type:"Poison", min_level:16 )
let sludgeBomb:Ability      = Ability(id:89, name:"Sludge Bomb",       base_value:86, type:"Poison", min_level:16 )
let toxicThread:Ability     = Ability(id:90, name:"Toxic Thread",      base_value:96, type:"Poison", min_level:16 ) 

//_____________________________________________________________________________________________________________________
//Habilidades de Ghost
let astralBarrage:Ability   = Ability(id:91, name:"Astral Barrage",    base_value:10, type:"Ghost", min_level:3 ) 
let curse:Ability           = Ability(id:92, name:"Curse",             base_value:11, type:"Ghost", min_level:3 ) 
let ominousWind:Ability     = Ability(id:93, name:"Ominous Wind",      base_value:13, type:"Ghost", min_level:3 ) 
let shadowBall:Ability      = Ability(id:94, name:"Shadow Ball",       base_value:18, type:"Ghost", min_level:3 ) 
let grudge:Ability          = Ability(id:95, name:"Grudge",            base_value:27, type:"Ghost", min_level:9 ) 
let nightmare:Ability       = Ability(id:96, name:"Nightmare",         base_value:29, type:"Ghost", min_level:9 ) 
let lick:Ability            = Ability(id:97, name:"Lick",              base_value:39, type:"Ghost", min_level:9 ) 
let shadowPunch:Ability     = Ability(id:98, name:"Shadow Punch",      base_value:43, type:"Ghost", min_level:9 ) 
let poltergeist:Ability     = Ability(id:99, name:"Poltergeist",       base_value:66, type:"Ghost", min_level:16 ) 
let trickOrTreat:Ability    = Ability(id:100, name:"Trick or Treat",   base_value:69, type:"Ghost", min_level:16 ) 
let spectralThief:Ability   = Ability(id:101, name:"Spectral Thief",   base_value:78, type:"Ghost", min_level:16 ) 
let astonish:Ability        = Ability(id:102, name:"Astonish",         base_value:99, type:"Ghost", min_level:16 ) 

//_____________________________________________________________________________________________________________________
//Habilidades de Dragon
let dracoMeteor:Ability     = Ability(id:103, name:"Draco Meteor",     base_value:15, type:"Dragon", min_level:3 ) 
let dragonClaw:Ability      = Ability(id:104, name:"Dragon Claw",      base_value:20, type:"Dragon", min_level:3 )
let outrage:Ability         = Ability(id:105, name:"Outrage",          base_value:21, type:"Dragon", min_level:3 )
let eternabeam:Ability      = Ability(id:106, name:"Eternabeam",       base_value:22, type:"Dragon", min_level:3 )
let dragonDarts:Ability     = Ability(id:107, name:"Dragon Darts",     base_value:35, type:"Dragon", min_level:9 ) 
let dragonPulse:Ability     = Ability(id:108, name:"Dragon Pulse",     base_value:37, type:"Dragon", min_level:9 )
let dragonRage:Ability      = Ability(id:109, name:"Dragon Rage",      base_value:41, type:"Dragon", min_level:9 )
let dragonBreath:Ability    = Ability(id:110, name:"Dragon Breath",    base_value:45, type:"Dragon", min_level:9 ) 
let dragonTail:Ability      = Ability(id:112, name:"Dragon Tail",      base_value:61, type:"Dragon", min_level:16 ) 
let roarOfTime:Ability      = Ability(id:113, name:"Roar of Time",     base_value:91, type:"Dragon", min_level:16 ) 
let clangingScales:Ability  = Ability(id:114, name:"Clanging Scales",  base_value:92, type:"Dragon", min_level:16 ) 
let dualChop:Ability        = Ability(id:115, name:"Dual Chop",        base_value:96, type:"Dragon", min_level:16 ) 

//_____________________________________________________________________________________________________________________
//Habilidades de Fairy
let charm:Ability           = Ability(id:116, name:"Charm",            base_value:8, type:"Fairy",  min_level:3 ) 
let drainingKiss:Ability    = Ability(id:117, name:"Draining Kiss",    base_value:21, type:"Fairy", min_level:3 ) 
let decorate:Ability        = Ability(id:118, name:"Decorate",         base_value:17, type:"Fairy", min_level:3 )
let lightOfRuin:Ability     = Ability(id:119, name:"Light of Ruin",    base_value:19, type:"Fairy", min_level:3 )
let geomancy:Ability        = Ability(id:120, name:"Geomancy",         base_value:35, type:"Fairy", min_level:9 ) 
let starFall:Ability        = Ability(id:121, name:"Star Fall",        base_value:31, type:"Fairy", min_level:9 ) 
let disarmingVoice:Ability  = Ability(id:122, name:"Disarming Voice",  base_value:39, type:"Fairy", min_level:9 ) 
let playRough:Ability       = Ability(id:123, name:"Play Rough",       base_value:67, type:"Fairy", min_level:9 ) 
let mistyExplosion:Ability  = Ability(id:124, name:"Misty Explosion",  base_value:87, type:"Fairy", min_level:16 ) 
let twinkleTackle	:Ability  = Ability(id:125, name:"Twinkle Tackle",   base_value:91, type:"Fairy", min_level:16 )
let sweetKiss:Ability       = Ability(id:126, name:"Sweet Kiss",       base_value:88, type:"Fairy", min_level:16 ) 
let spiritBreak	:Ability    = Ability(id:127, name:"Spirit Break",     base_value:100, type:"Fairy", min_level:16 )

//Lista de abilidades
let AbilityList:[Ability] = [
    tackle,        kick,          headBump,       push,             slap,           kneeKick,
    waterJet,      bubleBeam,     surf,           originPulse,      hydroPump,      waterfall,       sparklingAria,     crabhammer,      aquaTail,       maxGeyser,        steamEruption,      hydroCannon,
    rockHead,      magnetPull,    rockThrow,      rockBlast,        levitate,       rockStorm,       smackDown,         rollout,         rockShield,     earthquake,       tarShot,            stoneEdge,
    blaze,         steamEngine,   flameWheel,     heatCrash,        whiteSmoke,     fusionFlare,     turboBlaze,        flamethrower,    wallOfFire,     solarExplosion,   firePunch,          fireSpin,
    galvanize,     thunderbolt,   discharge,      electricTerrain,  electricSurge,  lightningRod,    gigavoltHavoc,     spark,           voltAbsorb,     motorDrive,       thunderShock,       voltTackle,
    metalBurst,    metalSound,    magnetBomb,     heavySlam,        shiftGear,      steelRoller,     gearUp,            doomDesire,      steelWing,      sunSteelStrike,   bulletPunch,        autotomize,
    cottonSpore,   cottonGuard,   leafStorm,      leafBlade,        bulletSeed,     overgrow,        ingrain,           needleArm,       gigaDrain,      frenzyPlant,      magicalLeaf,        grassyTerrain,
    poison,        poisonPoint,   poisonPowder,   maxOoze,          acid,           acidSpray,       acidDownpour,      corrosiveGas,    smog,           venoshock,        sludgeBomb,         toxicThread,
    astralBarrage, curse,         ominousWind,    shadowBall,       grudge,         nightmare,       lick,              shadowPunch,     poltergeist,    trickOrTreat,     spectralThief,      astonish,
    dracoMeteor,   dragonClaw,    outrage,        eternabeam,       dragonDarts,    dragonPulse,     dragonRage,        dragonBreath,    dragonTail,     roarOfTime,       clangingScales,     dualChop,
    charm,         drainingKiss,  decorate,       lightOfRuin,      geomancy,       starFall,        disarmingVoice,    playRough,       mistyExplosion, twinkleTackle,    sweetKiss,          spiritBreak
]

//Lista de strenghts
var strong: [String:[String]] = [
    "Water" : ["Fire", "Rock"],
    "Rock" : ["Fire"],
    "Fire" : ["Grass", "Steel"],
    "Electric" : ["Water"],
    "Steel" : ["Rock", "Fairy"],
    "Grass" : ["Water", "Ground", "Rock"],
    "Poison" : ["Grass", "Fairy"],
    "Ghost" : ["Ghost"],
    "Dragon" : ["Dragon"],
    "Fairy" : ["Dragon"],
    "Normal" : []
]

//Lista de weaknesses
var weak: [String:[String]] = [
    "Water" : ["Water", "Grass", "Dragon"],
    "Rock" : ["Ground", "Steel"],
    "Fire" : ["Fire", "Water", "Rock", "Dragon"],
    "Electric" : ["Electric", "Grass", "Dragon"],
    "Steel" : ["Fire", "Water", "Electric", "Steel"],
    "Grass" : ["Fire", "Grass", "Dragon"],
    "Poison" : ["Poison", "Ground", "Rock", "Ghost"],
    "Ghost" : ["Dark"],
    "Dragon" : ["Steel"],
    "Fairy" : ["Fire", "Poison", "Steel"],
    "Normal" : ["Rock", "Steel"]
]



let menuBattle:[String] = [
    "+----------------------------------+",
    "|               MENU               |",
    "+----------------------------------+",
    "|                                  |",
    "|                                  |",
    "|   1 -  Choose Pokemon            |",
    "|                                  |",
    "|                                  |",
    "|----------------------------------|",
    "|  0 - Back                        |",
    "+----------------------------------+",
    "        Select : ",
]

let selectYourPokemonMenuHead:[String] = [
    "+-------------------------------------------------+",
    "|                SELECT YOUR POKEMON              |",
    "+-------------------------------------------------+",
]

let selectOpponentPokemonMenuHead:[String] = [
    "+-------------------------------------------------+",
    "|                SELECT AN OPPONENT               |",
    "+-------------------------------------------------+",
]

let selectPokemonMenuFooter:[String] = [
    "|-------------------------------------------------|",
    "|  0 - Back                                       |",
    "+-------------------------------------------------+",
    "        Select : ",
]

let selectYourAbilityHead:[String] = [
    "+-------------------------------------------------+",
    "|                SELECT YOUR ABILITY              |",
    "+-------------------------------------------------+",
]

let showBattleHistoryHeader:[String] = [
    "+--------------------------------------------------------------+",
    "|                         BATTLE HISTORY                       |",
    "+--------------------------------------------------------------+",
]

let showBattleHistoryFooter:[String] = [
    "|--------------------------------------------------------------|",
    "|  0 - Back                                                    |",
    "+--------------------------------------------------------------+",
    "        Select : ",
]

let selectYourAbilityFooter:[String] = [
    "|-------------------------------------------------|",
    "|  0 - Back                                       |",
    "+-------------------------------------------------+",
    "        Select : ",
]

let Charmander = Pokemon(id: 1, name: "Charmander", level: 1, xp: 0, type: "Fire", hp:25, attack: 5, defense: 5, special_attack: 10, special_defense: 10, speed: 50, abilities: [1])

let Bulbasaur = Pokemon(id: 2, name: "Bulbasaur", level: 6, xp: 0, type: "Water", hp:25, attack: 5, defense: 5, special_attack: 10, special_defense: 10, speed: 30, abilities: [1,15])

let Pikachu = Pokemon(id: 3, name: "Pikachu", level: 40, xp: 0, type: "Lightning", hp:25, attack: 5, defense: 5, special_attack: 10, special_defense: 10, speed: 50, abilities: [1,43,45,50])

var Pokedex:[Pokemon] = [
    Charmander, Bulbasaur, Pikachu
]

var battleHistory:[Battle] = []


var pokemon1:Int = 0
var pokemon2:Int = 0

func choosePokemonName()->String{
    var name:String?
    
    print("▞▞▞▞ Insert the Pokemon Name ▚▚▚▚▚")
    print("-> ")
    //Receber input do utilizador
    name = readLine()
    
    return name!
}

func choosePokemonType ()->String{
    var type:String = ""
    var typeAux:Int?
    
    repeat{
        for x in pokemonTypes{
            print(x)
        }
        typeAux = Int(readLine()!)!
        if (typeAux! < 0 || typeAux! > 10){
            print("Error !")
            print("Incorrect Number")
        }
    } while typeAux! < 0 || typeAux! > 10
    
    //Verificar qual o type que foi escolido
    
    switch typeAux{
    case 1:
        type = "Water"
        break
    case 2:
        type = "Rock"
        break
    case 3:
        type = "Fire"
        break
    case 4:
        type = "Electric"
        break
    case 5:
        type = "Steel"
        break
    case 6:
        type = "Grass"
        break
    case 7:
        type = "Poison"
        break
    case 8:
        type = "Ghost"
        break
    case 9:
        type = "Dragon"
        break
    case 10:
        type = "Fairy"
        break
    default:
        break
    }
    return type
}

func choosePokemonAttack()->Int{
    var attack:Int = -1
    
    while (attack <= 0){
        attack = 0
        //Atribuir Atack
        print("⚔️⚔️⚔️⚔️ Insert the Atack Points ⚔️⚔️⚔️⚔️")
        print("-> ")
        attack = Int(readLine()!)!
        if (attack <= 0){
            print("Error !")
            print("The Atack points must be positive")
        }
    }
    
    return attack
} 

func choosePokemonDefense()->Int{
    var defense:Int = -1
    
    while (defense <= 0){
        defense = 0
        //Atribuir Defesa
        print("🛡️🛡️🛡️🛡️ Insert the Defense Points 🛡️🛡️🛡️🛡️")
        print("-> ")
        defense = Int(readLine()!)!
        if (defense <= 0){
            print("Error !")
            print("The Atack points must be positive")
        }
    }
    return defense
}

func choosePokemonSpecialAttack()->Int{
    var special_attack:Int = -1
    
    while (special_attack <= 0){
        special_attack = 0
        //Atribuir Special Atack
        print("💥💥💥💥 Insert Special Atack Points 💥💥💥💥")
        print("-> ")
        special_attack = Int(readLine()!)!
        if (special_attack <= 0){
            print("Error !")
            print("The Special Atack points must be positive")
        }
    }
    return special_attack
}

func choosePokemonSpecialDefense()->Int{
    var special_defense:Int = -1
    
    while (special_defense <= 0){
        special_defense = 0
        //Atribuir Special Defense
        print("🌟🌟🌟🌟 Insert Special Defense Points 🌟🌟🌟🌟")
        print("-> ")
        special_defense = Int(readLine()!)!
        if (special_defense <= 0){
            print("Error !")
            print("The Special Defense points must be positive")
        }
    }
    return special_defense
}

func choosePokemonSpeed()->Int{
    var speed:Int = -1
    while (speed <= 0 ){
        //Atribuir Special Defense
        print("💨💨💨💨 Insert The Speed of the Pokemon 💨💨💨💨")
        print("-> ")
        speed = Int(readLine()!)!
        if (speed <= 0 ){
            print("Error !")
            print("The speed of the Pokemon must be positive !")
        }
    }
    return speed
}

func choosePokemonHp()->Int{
    var hp:Int = -1
    
    while (hp <= 0 ){
        //Atribuir Special Defense
        print("❤️❤️❤️❤️   Insert The Health Points of the Pokemon  ❤️❤️❤️❤️")
        print("-> ")
        hp = Int(readLine()!)!
        if (hp <= 0 ){
            print("Error !")
            print("The HP of the Pokemon must be positive !")
        }
    }
    return hp
}

func choosePokemonStartAbilities()->Int{
    var ability:Int?
    var aux:Int?
    repeat{
        for x in choseGenericAbilitie{
            print(x)
        }
        aux = Int(readLine()!)!
        if (aux! < 0 || aux! > 6){
            print("Error !")
            print("Incorrect Number !")
        } else if (aux != 0) {
            ability = aux
        }
    }while aux! < 0 || aux! > 6
    
    
    return ability!
}

func showPokemonDetailed() {
    
}


func showAllPokemons()->Int{
    var pokemonToEdit:Int?
    
    print("▞▞▞▞    Your Pokemons    ▚▚▚▚▚")
    for x in Pokedex{
        print("\(x.getId()) - Name: \(x.getName()) Type: \(x.getType())")
    }
    pokemonToEdit = Int(readLine()!)!
    
    return pokemonToEdit!
}



func createPokemon(){
    //Variaveis do pokemon
    var id:Int
    var name:String?
    let level:Int = 1
    let xp:Int = 0
    var type:String
    var attack:Int = -1
    var defense:Int = -1
    var special_attack:Int = -1
    var special_defense:Int = -1
    var speed:Int = -1
    var hp:Int = -1
    var firstAbility:Int
    
    //Variaveis de funcao
    var _:Int
    var simNao:String?
    var simNaoAux:Bool = false
    
    //Calcular o id
    id = Pokedex.count + 1
    //Nome do Pokemon
    name = choosePokemonName()
    //Escolher o tipo do Pokemon
    type = choosePokemonType()
    //Definir os pontos de Ataque
    attack = choosePokemonAttack()
    //Definir os pontos de Defesa
    defense = choosePokemonDefense()
    //Definir os pontos de Ataque especial
    special_attack = choosePokemonSpecialAttack()
    //Definir os pontos de Defesa especial
    special_defense = choosePokemonSpecialDefense()
    //Definir o Spped
    speed = choosePokemonSpeed()
    //Definir o Hp
    hp = choosePokemonHp()
    //Atribuir as Abilidades iniciais
    firstAbility = choosePokemonStartAbilities()
    
    let abilities:[Int] = [firstAbility]
    
    //Verificar se o utilizador deseja mesmo criar o Pokemon
    while (simNaoAux == false){
        print("▞▞▞▞ Your new Pokemon: ▚▚▚▚▚")
        print("hp: \(hp) ")
        print("Name: \(name!) ")
        print("Type: \(type)")
        print("Atack: \(attack) ")
        print("Defense: \(defense)")
        print("Special Atack: \(special_attack) ")
        print("Special Defense: \(special_defense)")
        print("▞▞▞▞ Do you Wanna Keep the Pokemon ? (y/n) ▚▚▚▚▚")
        simNao = readLine()
        if(simNao == "y" || simNao == "Y"){
            //Criar o objeto pokemon
            
            let x:Pokemon = Pokemon(id:id, name:name! , level:level, xp:xp, type:type, hp:hp, attack:attack, defense:defense, special_attack:special_attack, special_defense:special_defense, speed:speed , abilities:abilities )
            Pokedex.append(x)
            print("▞▞▞▞ Pokemon Created with success !! ▚▚▚▚▚")
            simNaoAux = true
        }else if (simNao == "n" || simNao == "N"){
            simNaoAux = false
            print("▞▞▞▞ Pokemon Criation Aborted !! ▚▚▚▚▚")
        }
    }
}

func editPokemon(){
    //Variaveis do pokemon
    var pokemonId:Int?
    var pokemonName:String?
    var pokemonType:String?
    var pokemonAttack:Int = -1
    var pokemonDefense:Int = -1
    var pokemonSpecial_attack:Int = -1
    var pokemonSpecial_defense:Int = -1
    var speed:Int?
    var hp:Int?
    
    var fieldToEdit:Int?
    var aux:Bool = false
    
    //Escolher o pokemon a editar
    pokemonId = showAllPokemons()
    
    //Mostrar o pokemon escolhido
    showPokemonDetailed()
    
    //Mostar o menu que mostra os campos editaveis
    while aux == false{
        for x in campoAEditar{
            print(x)
        }
        fieldToEdit = Int(readLine()!)!
        var editPokemon:Pokemon?
        //Editar o pokemon
        for x in Pokedex {
            if x.getId() == pokemonId{
                editPokemon = x
            }
        }
        if (fieldToEdit == 0){
            aux = false
            print("▞▞▞▞ Your new Pokemon data: ▚▚▚▚▚")
            print("HP: \(editPokemon!.getHp()) ")
            print("Name: \(editPokemon!.getName()) ")
            print("Type: \(editPokemon!.getType())")
            print("Atack: \(editPokemon!.getAttack()) ")
            print("Defense: \(editPokemon!.getDefense())")
            print("Special Atack: \(editPokemon!.getSpecial_Attack()) ")
            print("Special Defense: \(editPokemon!.getSpecial_Defense())")
            
            var _:String = readLine()!
            break;
        }
        
        //Escolher o campo a editar
        switch fieldToEdit{
        case 1:
            pokemonName = choosePokemonName()
            editPokemon?.setName(name: pokemonName!)
            break
        case 2:
            pokemonType = choosePokemonType()
            editPokemon?.setType(type: pokemonType!)
            break
        case 3:
            pokemonAttack = choosePokemonAttack()
            editPokemon?.setAttack(attack: pokemonAttack)
            break
        case 4:
            pokemonDefense = choosePokemonDefense()
            editPokemon?.setDefense(defense: pokemonDefense)
            break
        case 5:
            pokemonSpecial_attack = choosePokemonSpecialAttack()
            editPokemon?.setSpecial_Attack(special_attack: pokemonSpecial_attack)
            break
        case 6:
            pokemonSpecial_defense = choosePokemonSpecialDefense()
            editPokemon?.setSpecial_Defense(special_defense: pokemonSpecial_defense)
            break
        case 7:
            speed = choosePokemonSpeed()
            editPokemon?.setSpeed(speed: speed!)
            break
        case 8:
            hp = choosePokemonHp()
            editPokemon?.setHp(hp: hp!)
            break
        default:
            break;
        }
    }
    
    
}

func deletePokemon(){
    var pokemonId:Int?
    //Escolher o pokemon a editar
    pokemonId = showAllPokemons()
    
    Pokedex.remove(at: pokemonId! - 1)
    
    print("You have deleted the pokemon!")
}

func show_Menu(Menu:[String]) -> Int{
    var escolhaAux: Int
    
    for x in Menu{
        print(x)
    }
    escolhaAux = Int(readLine()!)!
    
    return escolhaAux
}

func read_Menu(escolha:Int) -> Bool{
    var fechar:Bool = false
    switch escolha {
    case 0:
        fechar = true
        break
    case 1:
        createPokemon()
        break
    case 2:
        editPokemon()
        break
    case 3:
        deletePokemon()
        break
    case 4:
        let battle_escolha:Int = show_BattleMenu(menuBattle:menuBattle)
        switch battle_escolha {
        case 0:
            fechar = false
            break
        case 1:
            pokemon1 = choosePokemon()
            if pokemon1 == 0{
                break
            }
            pokemon2 = choosePokemon()
            if pokemon2 == 0{
                break
            }
            var pokemonInUse:Pokemon?
            var pokemonOpponent:Pokemon?
            for x in Pokedex {
                if x.getId() == pokemon1{
                    pokemonInUse = x
                }else if x.getId() == pokemon2{
                    pokemonOpponent = x
                }
            }
            
            let winner:Pokemon = simulateBattle(pokemonInUse:pokemonInUse!, pokemonOpponent:pokemonOpponent!)
            
            print("Winner of this fight -> \(winner.getName())")
            
            if (winner.getLevel() == 3 && winner.getAbilities().count < 2) || (winner.getLevel() == 9 && winner.getAbilities().count < 3) || (winner.getLevel() == 16 && winner.getAbilities().count < 4){
                addAbilitiesOnLvlUp(pokemon: winner, level: winner.getLevel())
            }
            
            var _:String = readLine()!
            break
        default:
            break
        }
        break
    case 5:
        if battleHistory.isEmpty {
            print("There are so battle stored.")
        }else{
            show_battleHistory()
        }
        break;
    default:
        break
    }
    return fechar
}

func show_battleHistory(){
    var escolhaAux: Int
    for x in showBattleHistoryHeader{
        print(x)
    }
    print(String(format:"|    %3@   |    %12@   |       %12@     |    %7@   |    %6@   |", "Id", "Winner", "Loser", "Xp Won", "Rounds"))
    for x in battleHistory{
        print(String(format:"|   %3i   |   %12@   |   %12@   |   %7i   |   %6i   |", x.getId(), x.getWinner().getName(), x.getLoser().getName(), x.getXpWon(), x.getRounds()))
    }
    
    
    for x in showBattleHistoryFooter{
        print(x)
    }
    escolhaAux = Int(readLine()!)!
}


func show_BattleMenu(menuBattle:[String]) -> Int{
    var escolhaAux: Int
    
    for x in menuBattle{
        print(x)
    }
    escolhaAux = Int(readLine()!)!
    
    
    return escolhaAux
}

func choosePokemon() -> Int{
    if pokemon1 != 0 {
        for x in selectOpponentPokemonMenuHead{
            print(x)
        }
    }else{
        for x in selectYourPokemonMenuHead{
            print(x)
        }
    }
    
    for x in Pokedex{
        if x.getId() != pokemon1 {
            print("|   \(x.getId()). \(x.getName()) [Level: \(x.getLevel()), Type: \(x.getType())]")
        }
    }
    
    for x in selectPokemonMenuFooter{
        print(x)
    }
    let choice:Int = Int(readLine()!)!
    
    return choice
}

func simulateBattle(pokemonInUse:Pokemon, pokemonOpponent:Pokemon) -> Pokemon {
    var fastestPokemon:Pokemon
    print(pokemonInUse.getName())
    print(pokemonOpponent.getName())
    var pokemon1Hp:Int = pokemonInUse.getHp()
    var pokemon2Hp:Int = pokemonOpponent.getHp()
    var winner:Pokemon
    var loser:Pokemon
    var lastPlayer:Int = pokemonInUse.getId()
    var rounds:Int = 0
    
    
    if pokemonInUse.getSpeed() >= pokemonOpponent.getSpeed() {
        fastestPokemon = pokemonInUse
    }else{
        fastestPokemon = pokemonOpponent
    }
    
    var damage:Int = 0
    if fastestPokemon.getId() == pokemonInUse.getId() {
        for x in selectYourAbilityHead {
            print(x)
        }
        var counter:Int = 0
        for x in pokemonInUse.getAbilities() {
            counter += 1
            for y in AbilityList{
                if x == y.getId(){
                    if y.getType() == "Normal" {
                        damage = calcNormalDamage(pokemon1: pokemonInUse, pokemon2: pokemonOpponent, ability: y)
                    }else{
                        damage = calcSpecialDamage(pokemon1: pokemonInUse, pokemon2: pokemonOpponent, ability: y)
                    }
                    if pokemonInUse.getLevel() >= y.getMin_level() {
                        print("|    \(counter). \(y.getName()) -> [Type: \(y.getType()), Damage: \(damage)]")
                    }
                }
            }
        }
        
        for x in selectYourAbilityFooter{
            print(x)
        }
        let choice:Int = Int(readLine()!)!
        
        for x in pokemonInUse.getAbilities() {
            if x == choice {
                for y in AbilityList{
                    if x == y.getId(){
                        if y.getType() == "Normal" {
                            damage = calcNormalDamage(pokemon1: pokemonInUse, pokemon2: pokemonInUse, ability: y)
                        }else{
                            damage = calcSpecialDamage(pokemon1: pokemonInUse, pokemon2: pokemonInUse, ability: y)
                        }
                        break;
                    }
                }
            }
        }
        
        if pokemon2Hp - damage > 0 {
            pokemon2Hp -= damage
        }else{
            pokemon2Hp = 0
        }
    }else{
        var abilities:[Int] = []
        for x in pokemonOpponent.getAbilities() {
            for y in AbilityList{
                if x == y.getId(){
                    if pokemonOpponent.getLevel() >= y.getMin_level() {
                        abilities.append(x)
                    }
                }
            }
        }
        let ability:Int = abilities.randomElement()!
        
        for x in pokemonOpponent.getAbilities() {
            if x == ability {
                for y in AbilityList{
                    if x == y.getId(){
                        if y.getType() == "Normal" {
                            print("\(pokemonOpponent.getName()) used \(y.getName()) !")
                            damage = calcNormalDamage(pokemon1: pokemonOpponent, pokemon2: pokemonInUse, ability: y)
                        }else{
                            damage = calcSpecialDamage(pokemon1: pokemonOpponent, pokemon2: pokemonInUse, ability: y)
                        }
                        break;
                    }
                }
            }
        }
        if pokemon1Hp - damage >= 0 {
            pokemon1Hp -= damage
        }else{
            pokemon1Hp = 0
        }
        
        lastPlayer = pokemonOpponent.getId()
    }
    rounds += 1
    while pokemon1Hp > 0 && pokemon2Hp > 0 {
        rounds += 1
        print("Your HP -> \(pokemon1Hp)")
        print("Opponent HP -> \(pokemon2Hp)")
        if lastPlayer == pokemonOpponent.getId() {
            for x in selectYourAbilityHead {
                print(x)
            }
            var counter:Int = 0
            for x in pokemonInUse.getAbilities() {
                counter += 1
                for y in AbilityList{
                    if x == y.getId(){
                        if y.getType() == "Normal" {
                            damage = calcNormalDamage(pokemon1: pokemonInUse, pokemon2: pokemonOpponent, ability: y)
                        }else{
                            damage = calcSpecialDamage(pokemon1: pokemonInUse, pokemon2: pokemonOpponent, ability: y)
                        }
                        if pokemonInUse.getLevel() >= y.getMin_level() {
                            print("|    \(counter). \(y.getName()) -> [Type: \(y.getType()), Damage: \(damage)]")
                        }
                    }
                }
            }
            
            for x in selectYourAbilityFooter{
                print(x)
            }
            let choice:Int = Int(readLine()!)!
            
            for x in pokemonInUse.getAbilities() {
                if x == choice {
                    for y in AbilityList{
                        if x == y.getId(){
                            if y.getType() == "Normal" {
                                print("You used \(y.getName()) !")
                                damage = calcNormalDamage(pokemon1: pokemonInUse, pokemon2: pokemonOpponent, ability: y)
                            }else{
                                damage = calcSpecialDamage(pokemon1: pokemonInUse, pokemon2: pokemonOpponent, ability: y)
                            }
                            break;
                        }
                    }
                }
            }
            
            if pokemon2Hp - damage >= 0 {
                pokemon2Hp -= damage
            }else{
                pokemon2Hp = 0
            }
            
            lastPlayer = pokemonInUse.getId()
        }else{
            var abilities:[Int] = []
            for x in pokemonOpponent.getAbilities() {
                for y in AbilityList{
                    if x == y.getId(){
                        if pokemonOpponent.getLevel() >= y.getMin_level() {
                            abilities.append(x)
                        }
                    }
                }
            }
            let ability:Int = abilities.randomElement()!
            
            for x in pokemonOpponent.getAbilities() {
                if x == ability {
                    for y in AbilityList{
                        if x == y.getId(){
                            if y.getType() == "Normal" {
                                print("\(pokemonOpponent.getName()) used \(y.getName()) !")
                                damage = calcNormalDamage(pokemon1: pokemonOpponent, pokemon2: pokemonInUse, ability: y)
                            }else{
                                damage = calcSpecialDamage(pokemon1: pokemonOpponent, pokemon2: pokemonInUse, ability: y)
                            }
                            break;
                        }
                    }
                }
            }
            
            if pokemon1Hp - damage > 0 {
                pokemon1Hp -= damage
            }else{
                pokemon1Hp = 0
            }
            
            lastPlayer = pokemonOpponent.getId()
        }
    }
    
    if pokemonInUse.getHp() <= 0 {
        winner = pokemonOpponent
        loser = pokemonInUse
    }else{
        winner = pokemonInUse
        loser = pokemonOpponent
    }
    
    pokemon1 = 0;
    pokemon2 = 0;
    
    let xpWon:Int = Int.random(in: 50..<150)
    
    if (winner.getXp() + xpWon) > (100 * winner.getLevel()) {
        winner.setXp(xp: 0)
        winner.setLevel(level: winner.getLevel() + 1)
        winner.setAttack(attack: winner.getAttack() + 3)
        winner.setDefense(defense: winner.getDefense() + 3)
        winner.setSpecial_Attack(special_attack: winner.getSpecial_Attack() + 5)
        winner.setSpecial_Defense(special_defense: winner.getSpecial_Defense() + 5)
        winner.setSpeed(speed: winner.getSpeed() + 3)
    }else{
        winner.setXp(xp: winner.getXp() + xpWon)
    }
    
    winner.setXp(xp: winner.getXp() + xpWon)
    battleHistory.append(Battle(id: battleHistory.count, winner: winner, loser: loser, xpWon: xpWon, rounds: rounds))
    return winner
}

func calcSpecialDamage(pokemon1: Pokemon, pokemon2: Pokemon, ability: Ability) -> Int {
    var damage:Int
    
    let weaknesses:[String] = strong[ability.getType()]!
    
    for x in weaknesses{
        if x == pokemon2.getType() {
            damage = (ability.getBase_value() + pokemon1.getSpecial_Attack() - pokemon2.getSpecial_Defense()) * 2
            if damage < 0 {
                damage = 0
            }
            return damage
        }
    }
    
    let strengths:[String] = weak[ability.getType()]!
    
    for x in strengths{
        if x == pokemon2.getType() {
            damage = (ability.getBase_value() + pokemon1.getSpecial_Attack() - pokemon2.getSpecial_Defense()) / 2
            if damage < 0 {
                damage = 0
            }
            return damage
        }
    }
    
    damage = ability.getBase_value() + pokemon1.getSpecial_Attack() - pokemon2.getSpecial_Defense()
    
    return damage
}

func calcNormalDamage(pokemon1: Pokemon, pokemon2: Pokemon, ability: Ability) -> Int {
    var damage:Int
    let weaknesses:[String] = strong[ability.getType()]!
    
    for x in weaknesses{
        if x == pokemon2.getType() {
            damage = (ability.getBase_value() + pokemon1.getAttack() - pokemon2.getDefense()) * 2
            if damage < 0 {
                damage = 0
            }
            return damage
        }
    }
    
    let strengths:[String] = weak[ability.getType()]!
    
    for x in strengths{
        if x == pokemon2.getType() {
            damage = (ability.getBase_value() + pokemon1.getAttack() - pokemon2.getDefense()) / 2
            if damage < 0 {
                damage = 0
            }
            return damage
        }
    }
    
    damage = ability.getBase_value() + pokemon1.getAttack() - pokemon2.getDefense()
    
    return damage
}

func addAbilitiesOnLvlUp(pokemon: Pokemon, level: Int){
    print("╔════════════════════════════════╗")
    print("║    You have leveled up!  🥳    ║")
    print("╚════════════════════════════════╝")
    print("╔════════════════════════════════╗")
    print("║    Select a new ability! 🤩    ║")
    print("╚════════════════════════════════╝")
    var counter:Int = 0
    switch pokemon.getType() {
    case "Water":
        switch level {
        case 3:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 3 && x.getType() == "Water" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 9:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 9 && x.getType() == "Water" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 16:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 16 && x.getType() == "Water" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        default:
            break
        }
        break
    case "Rock":
        switch level {
        case 3:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 3 && x.getType() == "Rock" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 9:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 9 && x.getType() == "Rock" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 16:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 16 && x.getType() == "Rock" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        default:
            break
        }
        break
    case "Fire":
        switch level {
        case 3:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 3 && x.getType() == "Fire" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 9:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 9 && x.getType() == "Fire" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 16:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 16 && x.getType() == "Fire" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        default:
            break
        }
        break
    case "Electric":
        switch level {
        case 3:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 3 && x.getType() == "Electric" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 9:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 9 && x.getType() == "Electric" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 16:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 16 && x.getType() == "Electric" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        default:
            break
        }
        break
    case "Steel":
        switch level {
        case 3:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 3 && x.getType() == "Steel" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 9:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 9 && x.getType() == "Steel" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 16:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 16 && x.getType() == "Steel" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        default:
            break
        }
        break
    case "Grass":
        switch level {
        case 3:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 3 && x.getType() == "Grass" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 9:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 9 && x.getType() == "Grass" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 16:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 16 && x.getType() == "Grass" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        default:
            break
        }
        break
    case "Poison":
        switch level {
        case 3:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 3 && x.getType() == "Poison" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 9:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 9 && x.getType() == "Poison" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 16:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 16 && x.getType() == "Poison" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        default:
            break
        }
        break
    case "Ghost":
        switch level {
        case 3:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 3 && x.getType() == "Ghost" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 9:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 9 && x.getType() == "Ghost" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 16:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 16 && x.getType() == "Ghost" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        default:
            break
        }
        break
    case "Dragon":
        switch level {
        case 3:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 3 && x.getType() == "Dragon" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 9:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 9 && x.getType() == "Dragon" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 16:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 16 && x.getType() == "Dragon" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        default:
            break
        }
        break
    case "Fairy":
        switch level {
        case 3:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 3 && x.getType() == "Fairy" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 9:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 9 && x.getType() == "Fairy" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        case 16:
            for x in AbilityList{
                counter += 1
                if x.getMin_level() == 16 && x.getType() == "Fairy" {
                    print("\(counter). \(x.getName())")
                }
            }
            break
        default:
            break
        }
        break
    default:
        break
    }
    
    let choice:Int = Int(readLine()!)!
    var abilities:[Int] = pokemon.getAbilities()
    abilities.append(choice)
    pokemon.setAbilities(abilities: abilities)
    
    print("New ability learned!")
    print("Id: \(AbilityList[choice-1].getId())")
    print("Name: \(AbilityList[choice-1].getName())")
    print("Damage: \(AbilityList[choice-1].getBase_value())")
}

/*
 * Motor principal do jogo
 * Como se fosse a funcao main
 * Serve so para mostrar o menu e receber qual a opcao escolhida pelo user
 */

repeat  {
    escolha = show_Menu(Menu:Menu)
    close = read_Menu(escolha:escolha)
} while !close
