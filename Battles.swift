//
//  Pokemon.swift
//  pokemonGame
//
//  Created by Formando ATEC on 07/10/2021.
//

import Foundation

class Battles {
    private var id:Int
    private var opponent1: Int
    private var opponent2: Int
    private var winner: Int

    init(id:Int, opponent1:Int, opponent2:Int, winner:Int){
        self.id = id
        self.opponent1 = opponent1
        self.opponent2 = opponent2
        self.winner = winner
    }

    func getOpponent1() -> Int{
        return self.opponent1
    }

    func getOpponent2() -> Int{
        return self.opponent2
    }

    func getWinner() -> Int{
        return self.winner
    }

    func setOpponent1(opponent1:Int){
        self.opponent1 = opponent1
    }

    func setOpponent2(opponent2:Int){
        self.opponent2 = opponent2
    }

    func setWinner(winner:Int){
        self.winner = winner
    }
}
